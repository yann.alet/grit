﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace Adysoft.Grit
{
    public static class Grit
    {
        public class Settings
        {
            public float scale = 1;
        }

        public class TextureInfo
        {
            public TextureInfo(Texture rt, int width, int height, bool always, int instanceId)
            {
                Texture = rt;
                Width = width;
                Height = height;
                Always = always;
                InstanceId = instanceId;
            }

            public Texture Texture { get; }
            public int Width { get; }
            public int Height { get; }
            public bool Always { get; }
            public int InstanceId { get; }
        }

        private static List<TextureInfo> m_QueuedRenderTextures;

        private static RenderTexture m_FinalTexture;  
        
        public static bool IsEnabled { get; set; }

        public static void OnUpdate(Settings settings)
        {
            if (!IsEnabled)
            {
                return;
            }

            if (m_QueuedRenderTextures != null && m_QueuedRenderTextures.Any())
            {
                if (m_FinalTexture != null)
                {
                    m_FinalTexture.Release();
                    m_FinalTexture = null;
                }

                var center = SceneView.lastActiveSceneView.camera.transform.position;
                
                m_FinalTexture = BuildFinalTexture(settings);
                var material = new Material(Shader.Find("Grit/QuadShader"));
                var bounds = new Bounds(center, new Vector3(100, 100, 100));
                material.SetTexture("debugTexture", m_FinalTexture);
                material.SetFloat("desiredWidth", m_FinalTexture.width);
                material.SetFloat("desiredHeight", m_FinalTexture.height);
                Graphics.DrawProcedural(material, bounds, MeshTopology.Triangles, 6);

                for(int i = 0; i < m_QueuedRenderTextures.Count; i++)
                {
                    if (!m_QueuedRenderTextures[i].Always)
                    {
                        var texture = m_QueuedRenderTextures[i--];
                        if (texture.Texture is RenderTexture rt && rt != null)
                        {
                            rt.Release();
                        }

                        m_QueuedRenderTextures.Remove(texture);
                    }
                }
            }
        }

        private static Rect GetRequiredSpace(Settings settings)
        {
            Rect rect = new Rect(0, 0, m_QueuedRenderTextures.Sum(t => Convert.ToInt32(t.Width*settings.scale)), m_QueuedRenderTextures.Max(t => Convert.ToInt32(t.Height*settings.scale)));
            return rect;
        }

        private static RenderTexture BuildFinalTexture(Settings settings)
        {
            Rect rect = GetRequiredSpace(settings);
            RenderTexture finalTexture = RenderTexture.GetTemporary((int)rect.width, (int)rect.height);
            finalTexture.enableRandomWrite = true;
            finalTexture.Create();

            int lastWidth = 0;
            for(int i = 0; i < m_QueuedRenderTextures.Count; i++)
            {
                var src = m_QueuedRenderTextures[i];
                var t = Resize(src, settings.scale);
                Graphics.CopyTexture(t, 0, 0, 0, 0, t.width, t.height, finalTexture, 0, 0, lastWidth, 0);
                t.Release();

                lastWidth += t.width;
            }

            return finalTexture;
        }

        private static RenderTexture Resize(TextureInfo textureInfo, float scale)
        {
            var oldRt = RenderTexture.active;
            RenderTexture rt = new RenderTexture(Convert.ToInt32(textureInfo.Width*scale), Convert.ToInt32(textureInfo.Height*scale), 24);
            rt.enableRandomWrite = true;
            rt.Create();
            Graphics.Blit(textureInfo.Texture, rt);
            RenderTexture.active = oldRt;
            return rt;
        }

        public static void DrawTexture(Texture texture, int width, int height, bool always = false)
        {
            if (texture == null || !IsEnabled)
            {
                return;
            }
            
            if (m_QueuedRenderTextures == null)
            {
                m_QueuedRenderTextures = new List<TextureInfo>();
            }

            for(int i = 0 ; i < m_QueuedRenderTextures.Count; i++)
            {
                if (m_QueuedRenderTextures[i].InstanceId == texture.GetInstanceID())
                {
                    if (m_QueuedRenderTextures[i].Texture is RenderTexture rt)
                    {
                        rt.Release();
                    }

                    m_QueuedRenderTextures.RemoveAt(i--);
                }
            }

            if (texture is RenderTexture renderTexture)
            {
                var textureCopy = new RenderTexture(texture.width, texture.height, renderTexture.depth, renderTexture.graphicsFormat);
                textureCopy.enableRandomWrite = true;
                textureCopy.Create();
                Graphics.CopyTexture(texture, 0, 0, 0, 0, texture.width, texture.height, textureCopy, 0, 0, 0, 0);
                m_QueuedRenderTextures.Add(new TextureInfo(textureCopy, width, height, always, texture.GetInstanceID()));
            }
            else if (texture is Texture2D texture2D)
            {
                Texture2D tex = new Texture2D(texture.width, texture.height, texture.graphicsFormat, TextureCreationFlags.None);
                for (int y = 0; y < texture2D.height; y++)
                {
                    for (int x = 0; x < texture2D.width; x++)
                    {
                        tex.SetPixel(x, y, texture2D.GetPixel(x, y));
                    }
                }
                tex.Apply(false, false);
                m_QueuedRenderTextures.Add(new TextureInfo(tex, width, height, always, texture.GetInstanceID()));
            }
        }

        public static RenderTexture CreateTiledTexture(int width, int height, GraphicsFormat? graphicsFormat = null)
        {
            if(!IsEnabled)
            {
                return null;
            }

            RenderTexture renderTexture = graphicsFormat == null ? new RenderTexture(width, height, 24) : new RenderTexture(width, height, 24, graphicsFormat.Value);
            renderTexture.enableRandomWrite = true;
            renderTexture.Create();
            return renderTexture;
        }

        public static void SetTile(RenderTexture tiledTexture, Texture tileTexture, int tileX, int tileY, float maxValue = 1)
        {
            if (!IsEnabled)
            {
                return;
            }

            if (tileTexture is RenderTexture texture && tileTexture.graphicsFormat == GraphicsFormat.R32_SFloat)
            {
                var src = new Texture2D(texture.width, texture.height);
                var convertedTexture = RenderTextureToTexture(texture);
                
                for (int x = 0; x < texture.width; x++)
                {
                    for (int y = 0; y < texture.height; y++)
                    {
                        var p = convertedTexture.GetPixel(x, y);
                        src.SetPixel(x,y, new Color(p.r/maxValue,p.r/maxValue,p.r/maxValue));
                    }
                }
                src.Apply(true, false);
                Graphics.CopyTexture(src, 0, 0, 0, 0, src.width, src.height, tiledTexture, 0, 0, tileX * src.width, tileY * src.height);
            }
            else
            {
                RectInt dstRegion = new RectInt(tileX * tileTexture.width, tileY * tileTexture.height, tileTexture.width, tileTexture.height); 
                Graphics.CopyTexture(tileTexture, 0, 0, 0, 0, tileTexture.width, tileTexture.height, tiledTexture, 0, 0, dstRegion.x, dstRegion.y);
            }
        }
        
        private static Texture2D RenderTextureToTexture(RenderTexture rt)
        {
            var oldRt = RenderTexture.active;
            RenderTexture.active = rt;
            Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.RFloat, false);
            tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            tex.Apply(false, false);
            RenderTexture.active = oldRt;
            return tex;
        }
    }
}
