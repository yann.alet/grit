﻿Shader "Grit/QuadShader"
{
    SubShader
    {
        Tags { "RenderType"="Opaque" }
		LOD 100
		ZTest Always
		Cull Off
		ZWrite On

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma enable_d3d11_debug_symbols

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				uint vid : SV_VertexID;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

			sampler2D debugTexture;
			float desiredWidth;
			float desiredHeight;

			v2f vert (appdata v)
            {
				float xPixelSize = 1.0f / _ScreenParams.x;
				float yPixelSize = 1.0f / _ScreenParams.y;
				float2 topRight = float2(-1,1) + float2(xPixelSize * desiredWidth, -(yPixelSize * desiredHeight));
				float2 bottomLeft = float2(-1, 1);

				float2 topTriangle[3] =
				{
					float2(bottomLeft.x, topRight.y),
					topRight,
					bottomLeft
				};

				float2 bottomTriangle[3] =
				{
					float2(topRight.x, bottomLeft.y),
					bottomLeft,
					topRight
				};

				float2 uv1[3] =
				{
					float2(0,1),
					float2(1,1),
					float2(0,0)
				};

				float2 uv2[3] =
				{
					float2(1,0),
					float2(0,0),
					float2(1,1)
				};

				bool isTop = v.vid < 3;

                v2f o;
				o.vertex = float4(isTop ? topTriangle[v.vid] : bottomTriangle[v.vid-3], 0, 1);
				o.uv = isTop ? uv1[v.vid] : uv2[v.vid-3];
                return o;
            }

            float4 frag (v2f i, out float outputDepth : SV_Depth) : SV_Target
            {
				outputDepth = 1;
				float3 col = tex2D(debugTexture, i.uv).rgb;
				return float4(col, 1);

            }
            ENDCG
        }
    }
}
