﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Adysoft.Grit
{
    [ExecuteAlways]
    public class GritBehaviour : MonoBehaviour
    {
        public float m_Scale = 1;


        void Update()
        {
            Grit.Settings settings = new Grit.Settings { scale = m_Scale };
            Grit.OnUpdate(settings);
        }

        private void OnDisable()
        {
            Grit.IsEnabled = false;
        }

        private void OnEnable()
        {
            Grit.IsEnabled = true;
        }
    }
}
