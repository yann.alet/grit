Simple library allowing you to debug textures similar to how you would use ImGUI.

Usage:

// Will draw a 512x512 quad with the specified texture.

Grit.DrawTexture(myTexture, 512, 512);
